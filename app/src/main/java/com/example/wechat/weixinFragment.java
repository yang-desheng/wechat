package com.example.wechat;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class weixinFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<String> mList = new ArrayList<>();
    private Context context;
    private VerticalAdapter adapter;
    public weixinFragment() { }

    private void initData() {
        mList.add("TL");
        mList.add("LGC");
        mList.add("SUP");
        mList.add("MAD");
        mList.add("ITZ");
        mList.add("PSG");
        mList.add("UOL");
        mList.add("R7");
        mList.add("LGD");
        mList.add("V3");
        mList.add("SN");
        mList.add("G2");
        mList.add("MCX");
        mList.add("DWG");
        mList.add("JDG");
        mList.add("RGE");
        mList.add("GEN");
        mList.add("FNC");
        mList.add("TSM");
        mList.add("TES");
        mList.add("DRX");
        mList.add("FLY");
    }

    private void initView_1() {
        context=this.getActivity();
        VerticalAdapter adapter = new VerticalAdapter(context);
        RecyclerView rcvVertical = recyclerView.findViewById(R.id.rcv_vertical);
        LinearLayoutManager managerVertical = new LinearLayoutManager(context);
        managerVertical.setOrientation(LinearLayoutManager.VERTICAL);
        rcvVertical.setLayoutManager(managerVertical);
        rcvVertical.setHasFixedSize(true);
        rcvVertical.setAdapter(adapter);
        adapter.setVerticalDataList(mList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab01, container, false);
        recyclerView=view.findViewById(R.id.rcv_vertical);
        initData();
        initView_1();
        return view;
    }
}
