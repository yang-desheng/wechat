package com.example.wechat;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Window;
import android.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ImageButton;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {
    private static final String TAG = MainActivity.class.getSimpleName();

    private List<String> mList = new ArrayList<>();
    private LinearLayout mTabWeiXin;
    private LinearLayout mTabFrd;
    private LinearLayout mTabContacts;
    private LinearLayout mTabSettings;

    private ImageButton mImgWeiXin;
    private ImageButton mImgFrd;
    private ImageButton mImgContacts;
    private ImageButton mImgSettings;

    private Fragment mTab01=new weixinFragment();
    private Fragment mTab02=new friendFragment();
    private Fragment mTab03=new contactFragment();
    private Fragment mTab04=new settingsFragment();
    FragmentManager fm = getFragmentManager();
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        initView();
        initFragment();
        initEvent();
        setSelect(0);
    }


    private void initEvent(){
        mTabWeiXin.setOnClickListener(this);
        mTabFrd.setOnClickListener(this);
        mTabContacts.setOnClickListener(this);
        mTabSettings.setOnClickListener(this);

    }

    private void initFragment(){
        fm=getFragmentManager();
        FragmentTransaction transaction=fm.beginTransaction();
        transaction.add(R.id.id_content,mTab01);
        transaction.add(R.id.id_content,mTab02);
        transaction.add(R.id.id_content,mTab03);
        transaction.add(R.id.id_content,mTab04);
        transaction.commit();
    }

    private void initView(){
        mTabWeiXin=(LinearLayout)findViewById(R.id.tab_weinxin);
        mTabFrd=(LinearLayout)findViewById(R.id.tab_frd);
        mTabContacts=(LinearLayout)findViewById(R.id.tab_contact);
        mTabSettings=(LinearLayout)findViewById(R.id.tab_settings);

        mImgWeiXin=(ImageButton)findViewById(R.id.imageButton1);
        mImgFrd=(ImageButton)findViewById(R.id.imageButton2);
        mImgContacts=(ImageButton)findViewById(R.id.imageButton3);
        mImgSettings=(ImageButton)findViewById(R.id.imageButton4);
    }

    private void setSelect(int i){
        FragmentTransaction transaction=fm.beginTransaction();
        hideFragment(transaction);
        switch (i){
            case 0:
                Log.d("setSelect","1");
                transaction.show(mTab01);
                mImgWeiXin.setImageResource(R.drawable.tab_weixin_pressed);
                break;
            case 1:
                transaction.show(mTab02);
                mImgFrd.setImageResource(R.drawable.tab_find_frd_pressed);
                break;
            case 2:
                transaction.show(mTab03);
                mImgContacts.setImageResource(R.drawable.tab_address_pressed);
                break;
            case 3:
                transaction.show(mTab04);
                mImgSettings.setImageResource(R.drawable.tab_settings_pressed);
                break;
            default:break;
        }
        transaction.commit();
    }

    private void hideFragment(FragmentTransaction transaction){
        transaction.hide(mTab01);
        transaction.hide(mTab02);
        transaction.hide(mTab03);
        transaction.hide(mTab04);
    }

    public void onClick(View v){
        Log.d("onClick","2");
        resetImgs();
        switch (v.getId()){
            case R.id.tab_weinxin:
                setSelect(0);
                break;
            case R.id.tab_frd:
                setSelect(1);
                break;
            case R.id.tab_contact:
                setSelect(2);
                break;
            case R.id.tab_settings:
                setSelect(3);
                break;
            default:
                break;
        }
    }

    public void resetImgs(){
        mImgWeiXin.setImageResource(R.drawable.tab_weixin_normal);
        mImgFrd.setImageResource(R.drawable.tab_find_frd_normal);
        mImgContacts.setImageResource(R.drawable.tab_address_normal);
        mImgSettings.setImageResource(R.drawable.tab_settings_normal);
    }
}

